package com.arkea.nv.test.data.manager.modele;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity(name = "model1")
public class Model1 {

    /**
     * Id.
     */
    @Id
    private Integer id;

    /**
     * Modele2.
     */
    @OneToOne
    private Model2 model2;

}
