package com.arkea.nv.test.data.manager.functional;

@FunctionalInterface
public interface IDeleteElement {

    /**
     * Permet la suppression en bdd d'un element.
     */
    void delete();
}
