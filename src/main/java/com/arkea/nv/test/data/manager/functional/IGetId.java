package com.arkea.nv.test.data.manager.functional;

@FunctionalInterface
public interface IGetId<T, I> {

    /**
     * Permet la récupération de l'id d'un élément.
     *
     * @param element Element persisté
     * @return Retourne l'id de l'élément.
     */
    I find(T element);
}
