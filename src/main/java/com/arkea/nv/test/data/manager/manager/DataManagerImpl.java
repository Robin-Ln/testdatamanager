package com.arkea.nv.test.data.manager.manager;

import com.arkea.nv.test.data.manager.functional.IDeleteElement;
import com.arkea.nv.test.data.manager.functional.IGetId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayDeque;
import java.util.Deque;

public class DataManagerImpl implements ITestDataManager {

    /**
     * Liste des items à supprimer.
     */
    private Deque<IDeleteElement> itemsToDelete;

    /**
     * Constructeur sans paramètre.
     */
    public DataManagerImpl() {
        this.itemsToDelete = new ArrayDeque<>();
    }

    @Override
    public <T, I> T findOrInsert(JpaRepository<T, I> jpaRepository, T element, IGetId<T, I> getId) {

        T result = jpaRepository
                // Récupération de l'élement
                .findById(getId.find(element))
                // Il il n'existe pas alors on le créer
                .orElseGet(() -> jpaRepository.save(element));

        // on l'ajoute à la liste des éléments à instaler
        itemsToDelete.addFirst(() -> jpaRepository.deleteById(getId.find(result)));

        return result;
    }

    @Override
    public void clear() {
        // on parcour la liste des éléments à supprimer
        while (!itemsToDelete.isEmpty()) {

            // suppression de l'élément
            itemsToDelete.pop().delete();
        }
    }
}
