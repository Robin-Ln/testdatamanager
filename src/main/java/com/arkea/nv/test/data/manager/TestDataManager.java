package com.arkea.nv.test.data.manager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class TestDataManager {

    /**
     * Lancement de l'application.
     *
     * @param args argument de l'app
     */
    public static void main(String[] args) {
        SpringApplication.run(TestDataManager.class, args);
    }

}
