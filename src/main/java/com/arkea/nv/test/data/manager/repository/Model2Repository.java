package com.arkea.nv.test.data.manager.repository;

import com.arkea.nv.test.data.manager.modele.Model2;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Model2Repository extends JpaRepository<Model2, Integer> {
}
