package com.arkea.nv.test.data.manager.modele;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity(name = "model2")
public class Model2 {

    /**
     * Id.
     */
    @Id
    private Integer id;


    /**
     * Label.
     */
    private String label;

}
