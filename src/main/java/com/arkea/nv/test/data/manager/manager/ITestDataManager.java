package com.arkea.nv.test.data.manager.manager;

import com.arkea.nv.test.data.manager.functional.IGetId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ITestDataManager {

    /**
     * Permet la récupération ou l'incertion en BDD.
     *
     * @param jpaRepository Répository Jpa
     * @param element       Object à persister
     * @param getId         Permet la récupération de l'id de l'objet
     * @param <T>           Type de l'objet à persister
     * @param <I>           Type de l'id de l'objet à persisté
     * @return Retourne l'objet à persister
     */
    <T, I> T findOrInsert(JpaRepository<T, I> jpaRepository, T element, IGetId<T, I> getId);

    /**
     * Permet la suppression des objets persistés.
     */
    void clear();
}
