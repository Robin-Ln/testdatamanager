package com.arkea.nv.test.data.manager.repository;

import com.arkea.nv.test.data.manager.modele.Model1;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Model1Repository extends JpaRepository<Model1, Integer> {
}
