package com.arkea.nv.test.data.manager;

import com.arkea.nv.test.data.manager.manager.DataManagerImpl;
import com.arkea.nv.test.data.manager.manager.ITestDataManager;
import com.arkea.nv.test.data.manager.modele.Model1;
import com.arkea.nv.test.data.manager.modele.Model2;
import com.arkea.nv.test.data.manager.repository.Model1Repository;
import com.arkea.nv.test.data.manager.repository.Model2Repository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestDataManager.class)
public class TestDataManagerTest {

    private boolean initialize = false;

    private Model1 mockModel1;

    private Model2 mockModel2;

    private ITestDataManager testDataManager = new DataManagerImpl();

    @Autowired
    private Model1Repository model1Repository;

    @Autowired
    private Model2Repository model2Repository;

    @Before
    public void init() {
        if (!initialize) {
            Model2 model2 = Model2.builder().id(1).label("label").build();
            Model1 model1 = Model1.builder().id(1).model2(model2).build();

            this.mockModel2 = testDataManager.findOrInsert(model2Repository, model2, Model2::getId);
            this.mockModel1 = testDataManager.findOrInsert(model1Repository, model1, Model1::getId);

            initialize = !initialize;
        }
    }

    @Test
    public void test() {

        Optional<Model1> model1 = model1Repository.findById(1);
        Optional<Model2> model2 = model2Repository.findById(1);

        if (model1.isPresent() && model2.isPresent()) {
            assertEquals(this.mockModel1, model1.get());
            assertEquals(this.mockModel2, model2.get());
        } else {
            fail();
        }

    }

    @After
    public void clear() {
        if (initialize) {
            testDataManager.clear();
            initialize = !initialize;
        }
    }

}
